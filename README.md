Docker image for Headless Factorio Server
---

To build:
Modify the server configuration json file and run:
`docker build -t factorio .`

To run:
`docker run --name [SERVERNAME] --restart unless-stopped -d -p 34197:34197/udp factorio:latest`
Creates a container named [SERVERNAME] from the image created above, with the correct port exposed, that will automatically restart with a system reboot.