# Johnathan Burns
# 2020-05-10
# Dockerfile to pull and run the latest stable factorio headless server

# Based on Debian due to Alpine incompatibility
FROM debian:latest

# User, utility, and folder setup
WORKDIR /opt
RUN apt-get update && apt-get install -y \
	curl \
	xz-utils
RUN ["useradd", "factorio", "-M", "-s", "/bin/false"]
RUN ["mkdir", "factorio"]
RUN ["chown", "factorio:factorio", "factorio"]

USER factorio

# Pulls the zipped file, extracts it, copies over the config file, and runs it to create a new map
RUN ["/bin/bash", "-c", "set -o pipefail && curl -sL https://factorio.com/get-download/stable/headless/linux64 | tar -Jxf -"]
COPY server-settings.json ./factorio/data/
RUN ["./factorio/bin/x64/factorio", "--create", "./factorio/saves/fsave.zip"]

# Port used by factorio
EXPOSE 34197/udp

# Run the server with the 
ENTRYPOINT ["/opt/factorio/bin/x64/factorio", "--start-server", "/opt/factorio/saves/fsave.zip", "--server-settings", "/opt/factorio/data/server-settings.json"]
